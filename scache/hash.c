#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#include "../sc_pool.h"
#include "hash.h"


unsigned hash(const char *str, unsigned size)
{
    /*hash func*/
    return 0;
}

hash_table_t *hash_table_init(sc_pool_t pool, unsigned size)
{
    hash_table_t *ht;
    int i;
    assert (size > 0);

    ht = sc_pool_alloc(pool, sizeof(hash_table_t), __FILE__, __LINE__);
    ht->table = sc_pool_calloc(pool, size, sizeof(hash_entry_t ), __FILE__, __LINE__);
    
    
    for(i = 0; i < size; i++){
        ht->table[i] = NULL;
    }
    ht->size = size;
    ht->count = 0;

    return ht;
}

int insert_hash(sc_pool_t pool, hash_table_t *ht, char *key, char *data,
               unsigned length, unsigned lifetime, short mode)
{
    unsigned value;

    if(get_hash(ht, key) != NULL){
        if(mode == MODE_ADD)
            return 0;
        else 
            remove_hash(ht, key);
        
    }
            
    time_t currtime;
    currtime = time((time_t *)NULL);
    hash_entry_t he = sc_pool_alloc(pool, sizeof (*he), 
                                     __FILE__, __LINE__);
    
    value = hash(key, ht->size -1);
    he->key = key;
    he->data = data;
    he->length = length;
    he->create = currtime;
    he->expried = currtime + lifetime;
    he->next = ht->table[value] != NULL? ht->table[value]: NULL;

    ht->table[value] = he;

    return 1;
}

hash_entry_t get_hash(hash_table_t *ht, char *key)
{
    hash_entry_t he;
    unsigned value = hash(key, ht->size - 1);

    for (he = ht->table[value]; he != NULL; he = he->next){
        if (strcmp(he->key, key) == 0){
            return he;
        }
    }
    return NULL;
}

int hash_remove(hash_table_t *ht, char *key)
{
    unsigned value;
    value = hash(key, ht->size - 1);

    hash_entry_t he, tmp;
    he = ht->table[value];

    if (he && strcmp (he->key, key)  == 0) {
        tmp = he->next;
        /*free the entry*/
        ht->table[value] = tmp ;
        return 1;
    }else {
        while (he) {
            if(he->next && strcmp(he->next->key,key) == 0) {
                tmp = he->next;
                he->next = he->next->next;
                /*must to have free(tmp);*/

                return 1;
            }
            he = he->next;
        }
    }
    return -1;
}
int free_hash_table(sc_pool_t pool)
{
    /*somethings don't work here?*/
    sc_pool_free(pool);
    return 1;
}

int walk_hash_table(hash_table_t *ht)
{
    int i;
    hash_entry_t he;
    assert(ht != NULL);
    printf("----------walk in the hashtable------------\n");
    for (i = 0; i<ht->size; i++){
        he = ht->table[i];
        while(he != NULL){
            printf("Entry %d: key:%s, value:%s\n", i, he->key, he->data);
            he = he->next;
        }
    }
    return 0;
}

int print_hash(hash_entry_t node)
{
    if (node == NULL){
        printf("Error node :%s,%d\n", __FILE__, __LINE__);
        return -1;
    }
    printf("node key is:%s, value is:%s, length : %d,created:%d,\
           expired:%d\n",node->key, node->data, node->length,
           node->create, node->expried);
    return 0;
}


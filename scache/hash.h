#ifndef _HASH_H_
#define _HASH_H_

#define MODE_ADD 10
typedef struct hash_entry_s *hash_entry_t;
struct hash_entry_s {
    char *key;
    char *data;

    size_t length;
    unsigned create;
    unsigned expried;
    hash_entry_t next;
};

typedef struct hash_table_s {
    hash_entry_t *table;
    unsigned size;
    unsigned count;
}hash_table_t;

unsigned hash(const char *str, unsigned size);

hash_table_t *hash_table_init(sc_pool_t pool, unsigned size);

int insert_hash(sc_pool_t pool, hash_table_t *hashtable, char *key, char *data,
               unsigned length, unsigned lifetime, short mode);

int hash_remove(hash_table_t *hashtable, char *key);

int free_hash_table(sc_pool_t pool);

int walk_hash_table(hash_table_t *hashtable);

hash_entry_t get_hash(hash_table_t *hashtable, char *key);

#endif
